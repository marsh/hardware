# Copyright 2008, 2009 Ingmar Vanhassel <ingmar@exherbo.org>
# Copyright 2009-2017 Wulf C. Krueger <philantrop@exherbo.org>
# Copyright 2019 Markus Schmits <marsh.exherbo@gmail.com>
# Distributed under the terms of the GNU General Public License v2
# Based in part upon 'hplip-2.8.9.ebuild' from Gentoo, which is:
#     Copyright 1999-2008 Gentoo Foundation.

require option-renames [ renames=[ 'policykit polkit' ] ]
require python [ blacklist=3.7 multibuild=false ] udev-rules 
require autotools [ supported_autoconf=[ 2.5 ] supported_automake=[ 1.16 1.15 1.14 ] ]

export_exlib_phases pkg_pretend src_prepare src_configure src_install pkg_postinst

SUMMARY="HP Linux Imaging and Printing System"
DESCRIPTION="
HPLIP (HP Linux Imaging and Printing) is a complete single and multi-function
printing device connectivity solution for users of Linux. Supported features
include a toolbox with status and maintenance functions, scanning, a CUPS
backend that supports bidirectional I/O, and photo card unloading. It includes
HPIJS 2.x and supports over 1000 HP printer models including Deskjet, Business
Inkjet, Photosmart, Business Inkjet, PSC, Officejet, Mono/Color LaserJet, and
LaserJet Multifunction Peripheral (MFP).
"
HOMEPAGE="https://developers.hp.com/hp-linux-imaging-and-printing"
DOWNLOADS="mirror://sourceforge/${PN}/${PNV}.tar.gz"

BUGS_TO="marsh.exherbo@gmail.com"
REMOTE_IDS="freecode:${PN}"

UPSTREAM_RELEASE_NOTES="${HOMEPAGE}/release_notes"
UPSTREAM_DOCUMENTATION="${HOMEPAGE}/install/manual/distros/other"

LICENCES="GPL-2 MIT BSD-3"
SLOT="0"
MYOPTIONS="
    dbus
    doc
    fax [[ requires = dbus description = [ Enable fax support ] ]]
    gui
    polkit [[ description = [ Allow the commandline tools to be run by regular users ] ]]
    scanner
    snmp
    snmp? ( ( providers: libressl openssl ) [[ number-selected = exactly-one ]] )
    ( providers: ijg-jpeg jpeg-turbo ) [[ number-selected = exactly-one ]]
"

DEPENDENCIES="
    build+run:
        app-text/ghostscript[>=8.63]
        gnome-bindings/pygobject:=[python_abis:*(-)?]
        net-print/cups[>=1.3.9]
        virtual/usb:1
        dbus? ( sys-apps/dbus )
        fax? (
            sys-apps/dbus
            dev-python/dbus-python[>=0.83.0][python_abis:*(-)?]
            dev-python/reportlab[python_abis:*(-)?]
        )
        gui? (
            x11-libs/qtbase:5
            dev-python/PyQt5[python_abis:*(-)?]
        )
        polkit? ( sys-auth/polkit:1 )
        providers:ijg-jpeg? ( media-libs/jpeg:=[>=6b] )
        providers:jpeg-turbo? ( media-libs/libjpeg-turbo )
        scanner? (
            media-gfx/sane-backends
            dev-python/Pillow[>=1.1.6][python_abis:*(-)?]
        )
        snmp? (
            net/net-snmp
            providers:libressl? ( dev-libs/libressl:= )
            providers:openssl? ( dev-libs/openssl )
        )
"

hplip_pkg_pretend() {
    local f nuke_f=()
    for f in "${ROOT%%/}"/etc/udev/rules.d/{55-hpmud.rules,56-hpmud_support.rules}; do
        [[ -e ${f} ]] && nuke_f+=( "${f}" )
    done

    if [[ -n ${nuke_f[@]} ]]; then
        echo >&2
        ewarn "The following system provided udev rules have moved to 2>/dev/null) ${UDEVRULESDIR}:"
        for f in "${nuke_f[@]}"; do
            ewarn "    ${f}"
        done
        ewarn "You should move any changes you may have made to them to a custom file in ${f%/*}/ and remove the original."
    fi
}

hplip_src_prepare() {
    edo sed -e "s:\.\./share:../&:" \
            -e "s:-I/usr/include/libusb-1.0:-I/usr/$(exhost --target)/include/libusb-1.0:g" \
            -i Makefile.am \
            -i Makefile.in

    autotools_src_prepare
}

hplip_src_configure() {
    econf \
        --with-hpppddir=/usr/share/ppd/${PN} \
        --with-drvdir=/usr/share/ppd/${PN}-drv \
        --with-cupsbackenddir=$(cups-config --serverbin)/backend \
        --with-cupsfilterdir=$(cups-config --serverbin)/filter \
        --enable-cups-ppd-install \
        --enable-cups-drv-install \
        --disable-udev_sysfs_rules \
        --disable-libusb01_build \
        --disable-hpcups-install \
        --enable-new-hpcups \
        --disable-qt4 \
        $(option_enable dbus dbus-build) \
        $(option_enable doc doc-build) \
        $(option_enable fax fax-build) \
        $(option_enable gui qt5) \
        $(option_enable polkit policykit) \
        $(option_enable scanner scan-build) \
        $(option_enable snmp network-build)
}

hplip_src_install() {
    default

    if option scanner ; then
        # Moving dll.conf which originally belongs to sane-backends out of the way.
        edo mv "${IMAGE}"etc/sane.d/dll.conf "${TEMP}"hplip_dll.conf
        edo rmdir "${IMAGE}"etc/sane.d
    fi

    edo sed -i -e 's/SYSFS{/ATTRS{/g' "${IMAGE}"/etc/udev/rules.d/*.rules
    dodir "${UDEVRULESDIR}"
    edo mv "${IMAGE}"/etc/udev/rules.d/* "${IMAGE}"/"${UDEVRULESDIR}"

    if option gui; then
        local empty=( "${IMAGE}"/usr/share/hplip/ui4/plugins )
        [[ -d ${empty[@]} ]] && edo rmdir "${empty[@]}"
    else
        local empty=( "${IMAGE}"/usr/share/hplip/data/images/{*/,} )
        [[ -d ${empty[@]} ]] && edo rmdir "${empty[@]}"
    fi

    edo mv "${IMAGE}"/usr/lib/systemd "${IMAGE}"/usr/$(exhost --target)/lib/
    edo rm -r "${IMAGE}"/usr/lib

    local empty=(
        "${IMAGE}"/etc/udev
        "${IMAGE}"/usr/share/hal
    )

    for dir in "${empty[@]}"; do
        [[ -d ${dir} ]] && edo rm -r "${dir}"
    done

    keepdir /var/{lib,log}/hp
    keepdir /var/log/hp/tmp
}

hplip_pkg_postinst() {
    if option scanner ; then
        # Merging hplip's dll.conf entry into the original one.
        cp "${ROOT}"etc/sane.d/dll.conf "${TEMP}" || eerror "copying dll.conf failed"
        cat "${TEMP}"hplip_dll.conf >> "${TEMP}"dll.conf \
            || eerror "adding hplip's dll.conf to the original one failed."
        cp "${TEMP}"dll.conf "${ROOT}"etc/sane.d/dll.conf \
            || eerror "writing back dll.conf failed"
    fi
}

