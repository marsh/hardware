# Copyright 2009-2010 Ingmar Vanhassel <ingmar@exherbo.org>
# Copyright 2010-2016 Wulf C. Krueger <philantrop@exherbo.org>
# Copyright 2010 Piotr Jaorszyński <p.jaroszynski@gmail.com>
# Distributed under the terms of the GNU General Public License v2

# Note to my future self: Find status information here: http://www.nvidia.com/object/unix.html

require makeself alternatives udev-rules

export_exlib_phases src_unpack src_prepare src_configure src_install pkg_postinst

SUMMARY="NVIDIA X11 driver and GLX libraries"
HOMEPAGE="https://www.nvidia.com/"

AMD64_PNV="NVIDIA-Linux-x86_64-${PV}.run"
X86_PNV="NVIDIA-Linux-x86-${PV}.run"

DOWNLOADS="
    listed-only:
        https://us.download.nvidia.com/XFree86/Linux-x86_64/${PV}/${AMD64_PNV}
        https://download.nvidia.com/XFree86/Linux-x86_64/${PV}/${AMD64_PNV}
        ftp://download.nvidia.com/XFree86/Linux-x86_64/${PV}/${AMD64_PNV}

        https://us.download.nvidia.com/XFree86/Linux-x86/${PV}/${X86_PNV}
        https://download.nvidia.com/XFree86/Linux-x86/${PV}/${X86_PNV}
        ftp://download.nvidia.com/XFree86/Linux-x86/${PV}/${X86_PNV}
"

BUGS_TO="philantrop@exherbo.org"

LICENCES="NVIDIA"
SLOT="0"
MYOPTIONS="
    doc
    tools [[ description = [ Install nvidia-settings GUI application ] ]]
"

if ever at_least 364.12; then
    MYOPTIONS+="
        wayland
    "
fi

DEPENDENCIES="
    run:
        x11-libs/libX11
        x11-libs/libXau
        x11-libs/libXext
       !x11-dri/eclectic-opengl
        tools? (
            dev-libs/atk
            dev-libs/glib:2
            x11-libs/pango
        )
    suggestion:
        x11-libs/libvdpau[>=0.3]
"

if ever at_least 364.12; then
    DEPENDENCIES+="
        run:
            wayland? ( sys-libs/wayland )
    "
fi

if ever at_least 346.16; then
    DEPENDENCIES+="
        run:
            tools? ( x11-libs/gtk+:3 )
    "
else
    DEPENDENCIES+="
        run:
            tools? ( x11-libs/gtk+:2 )
    "
fi

if ! ever at_least 349.16; then
    DEPENDENCIES+="
        run:
            x11-libs/libXdmcp
            x11-libs/libXv
            x11-libs/libXvMC
    "
fi

NVIDIA_DRIVER_MODULES=(
    "nvidia /kernel/drivers/video"
    "nvidia-uvm /kernel/drivers/video"
)

if ever at_least 358.16; then
    NVIDIA_DRIVER_MODULES+=( "nvidia-modeset /kernel/drivers/video" )
fi

if ever at_least 384.59; then
    NVIDIA_DRIVER_MODULES+=( "nvidia-drm /kernel/drivers/video" )
fi

nvidia-drivers_generate_dkms_modules() {
    local module n=0
    for module in "${NVIDIA_DRIVER_MODULES[@]}"; do
        local args=(${module})
        echo "BUILT_MODULE_NAME[${n}]=\"${args[0]}\""
        echo "DEST_MODULE_LOCATION[${n}]=\"${args[1]}\""
        let ++n
    done
}

nvidia-drivers-legacy_src_unpack() {
    case "$(exhost --target)" in
        x86_64*)
            unpack_makeself ${AMD64_PNV}
            ;;
        i*86-*)
            unpack_makeself ${X86_PNV}
            ;;
    esac
}

nvidia-drivers-legacy_src_prepare() {
    local desktop_path=usr/share/applications/nvidia-settings.desktop
    desktop_path=nvidia-settings.desktop

    default

    edo sed -e "s:__UTILS_PATH__:/usr/$(exhost --target)/bin:" \
        -e 's:__PIXMAP_PATH__:/usr/share/pixmaps:' \
        -i "${desktop_path}"
}

nvidia-drivers-legacy_src_configure() {
    moduledir=/usr/$(exhost --target)/lib/xorg/modules

    # configure dkms
    local dkms_modules=$(nvidia-drivers_generate_dkms_modules | sed -e 's/$/\\/g;$s/\\$//')

    edo sed \
        -e "s:__VERSION_STRING:${PV}:" \
        -e "s:__JOBS:${EXJOBS}:" \
        -e "s:__EXCLUDE_MODULES::" \
        -e 's/^__DKMS_MODULES$/#&/' \
        -e "/^#__DKMS_MODULES$/a${dkms_modules}" \
        -i kernel/dkms.conf
}

src_install_256() {
    local current_target

    # libvdpau{,_trace}.so* collide with x11-libs/libvdpau
    edo sed -e '/libvdpau\.so/d' -e '/libvdpau_trace\.so/d' -i .manifest

    # we do not install nvidia-installer, remove the manpage
    edo sed -e '/nvidia-installer\.1\.gz/d' -i .manifest

    # use only gtk3 for nvidia-settings in recent versions
    if ever at_least 346.16; then
        edo sed -e '/libnvidia-gtk2.so/d' -i .manifest
    fi

    # remove disabled stuff
    if ! option tools ; then
        if ever at_least 346.16 ; then
            edo sed -e '/libnvidia-gtk3.so/d' -i .manifest
        else
            edo sed -e '/libnvidia-gtk2.so/d' -i .manifest
        fi
        edo sed -e '/nvidia-settings/d' -i .manifest
    fi

    if ever at_least 364.12 ; then
        if ! option wayland ; then
            edo sed \
                -e '/10_nvidia_wayland.json/d' \
                -e '/libnvidia-egl-wayland.so/d' \
                -i .manifest
        fi
    fi

    # collides with files installed by dev-libs/ocl-icd
    if ! ever at_least 341.00 ; then
        edo sed \
            -e '/libOpenCL.so/d' \
            -i .manifest
    fi

    # fill nvidia_icd.json.template and rename
    if ever at_least 384.59 ; then
        edo sed \
            -e 's:__NV_VK_ICD__:libGLX_nvidia.so.0:g' \
            -i nvidia_icd.json.template
        edo sed \
            -e 's:nvidia_icd.json.template:nvidia_icd.json:g' \
            -i .manifest
        edo mv nvidia_icd.json.template nvidia_icd.json
    fi

    # parse the .manifest file to figure out where to install stuff
    while read line ; do
        line=( $line )
        case ${line[2]} in
            UTILITY_BINARY)
                dobin ${line[0]}
                ;;
            MANPAGE)
                edo gunzip ${line[0]}
                doman ${line[0]%.gz}
                ;;
            XMODULE_SHARED_LIB|GLX_MODULE_SHARED_LIB)
                exeinto ${moduledir}/${line[3]}
                doexe ${line[0]}
                ;;
            XMODULE_SYMLINK|GLX_MODULE_SYMLINK)
                ;;
            GLX_CLIENT_LIB|EGL_CLIENT_LIB)
                ever at_least 390.42 && [[ ${line[4]} == NON_GLVND ]] && continue
                ;& # fallthrough
            XLIB_SHARED_LIB|UTILITY_LIB|ENCODEAPI_LIB|NVCUVID_LIB)
                if [[ ${line[3]} == "NATIVE" ]] ; then
                    dolib.so ${line[0]}
                fi
                ;;
            XLIB_SYMLINK|UTILITY_LIB_SYMLINK|ENCODEAPI_LIB_SYMLINK|NVCUVID_LIB_SYMLINK|OPENGL_SYMLINK)
                if [[ ${line[3]} == "NATIVE" ]] ; then
                    dosym ${line[4]} /usr/$(exhost --target)/lib/${line[0]}
                fi
                ;;
            OPENGL_LIB|VDPAU_LIB|CUDA_LIB|GLVND_LIB|OPENCL_LIB|NVIFR_LIB)
                if [[ ${line[3]} == "NATIVE" ]] ; then
                    exeinto /usr/$(exhost --target)/lib/${line[4]}
                    doexe ${line[0]}
                fi
                ;;
            VDPAU_SYMLINK|CUDA_SYMLINK|OPENCL_LIB_SYMLINK)
                if [[ ${line[3]} == "NATIVE" ]] ; then
                    dosym ${line[5]} /usr/$(exhost --target)/lib/${line[4]}/${line[0]}
                fi
                ;;
            TLS_LIB)
                if [[ ${line[3]} == "NATIVE" ]] ; then
                    exeinto /usr/$(exhost --target)/lib/${line[5]}
                    doexe ${line[0]}
                fi
                ;;
            DOT_DESKTOP)
                if option tools ; then
                    insinto /usr/share/applications
                    doins ${line[0]}
                fi
                ;;
            APPLICATION_PROFILE)
                if option tools ; then
                    insinto /usr/share/nvidia
                    doins ${line[0]}
                fi
                ;;
            XORG_OUTPUTCLASS_CONFIG)
                insinto /usr/share/X11/xorg.conf.d
                doins ${line[0]}
                ;;
            CUDA_ICD)
                insinto /etc/OpenCL/vendors
                doins ${line[0]}
                ;;
            VULKAN_ICD_JSON)
                insinto /usr/share/vulkan/icd.d
                doins ${line[0]}
                ;;
            GLVND_EGL_ICD_JSON)
                insinto /usr/share/glvnd/egl_vendor.d
                doins ${line[0]}
                ;;
            EGL_EXTERNAL_PLATFORM_JSON)
                insinto /usr/share/glvnd/egl_vendor.d
                doins ${line[0]}
                ;;
        esac
    done < .manifest

    if ever at_least 381.22 ; then
        local modules=( egl opengl nvifr )
        for module in "${modules[@]}"; do
            edo mv "${IMAGE}"/usr/$(exhost --target)/lib/MODULE\:${module}/* \
                "${IMAGE}"/usr/$(exhost --target)/lib/
            edo rmdir "${IMAGE}"/usr/$(exhost --target)/lib/MODULE\:${module}
        done
    fi

    # nvidia-settings icon
    if option tools ; then
        insinto /usr/share/pixmaps
        doins nvidia-settings.png
    fi

    # docs
    dodoc pkg-history.txt NVIDIA_Changelog README.txt
    if option doc; then
        docinto html
        dodoc -r html/*
    fi

    # kernel module source
    local modules_sources=/usr/src/${PNV}
    insinto ${modules_sources}
    doins -r kernel/*

    local arch=$(exhost --target)

    # we need to map to the kernel arch here
    case "${arch}" in
        i*86-*)
            arch=x86
            ;;
        *)
            arch=${arch/-*}
            ;;
    esac

    if ever at_least 355.06; then
        edo mv "${IMAGE}"${modules_sources}/nvidia/nv-kernel{,-${arch}}.o_binary
        edo sed \
            -e "s/nv-kernel\\.o/nv-kernel-\$(ARCH).o/" \
            -i "${IMAGE}"${modules_sources}/nvidia/nvidia.Kbuild

        if ever at_least 358.16; then
            edo mv "${IMAGE}"${modules_sources}/nvidia-modeset/nv-modeset-kernel{,-${arch}}.o_binary
            edo sed \
                -e "s/nv-modeset-kernel\\.o/nv-modeset-kernel-\$(ARCH).o/" \
                -i "${IMAGE}"${modules_sources}/nvidia-modeset/nvidia-modeset.Kbuild
        fi
    else
        edo mv \
            "${IMAGE}"${modules_sources}/nv-kernel{,-${arch}}.o
        edo sed \
            -e "s/nv-kernel\\.o/nv-kernel-\$(ARCH).o/" \
            -i "${IMAGE}"${modules_sources}/Makefile
    fi
}

nvidia_alternatives() {
    local host=$(exhost --target)
    local libs=( libGL libEGL libGLESv1_CM libGLESv2 )
    # the 390 branch bundles libglvnd, so we need to alternative' everything from that
    ever at_least 390.42 && libs+=( libGLX libOpenGL libGLdispatch )

    edo mkdir "${IMAGE}"/usr/${host}/lib/opengl

    local lib path_old path_new soname
    for lib in "${libs[@]}"; do
        path_new=/usr/${host}/lib/opengl/${lib}-nvidia.so
        path_old=$(readlink -f "${IMAGE}"/usr/${host}/lib/${lib}.so* | head -n1)

        edo mv \
            "${path_old}" \
            "${IMAGE}"/${path_new}

        local objdump=$(exhost --tool-prefix)objdump
        soname=$(edo ${objdump} -p "${IMAGE}"/${path_new} | sed -n 's/^ *SONAME *//p')

        # clean up the leftover symlinks
        nonfatal edo rm "${IMAGE}"/usr/${host}/lib/{${lib}.so,${soname}}

        alternatives+=(
            /usr/${host}/lib/${lib}.so ${path_new}
            /usr/${host}/lib/${soname} ${path_new}
        )
    done

    alternatives+=(${moduledir}/extensions/libglx.so{,.${PV}})
}

nvidia-drivers-legacy_src_install() {
    local current_target

    src_install_256

    hereenvd 40nvidia <<EOF
LDPATH=/usr/@TARGET@/lib/vdpau
EOF

    if option tools ; then
        insinto /etc/X11/xinit/xinitrc.d
        hereins 95-nvidia-settings <<EOF
#!/bin/sh

/usr/$(exhost --target)/bin/nvidia-settings --load-config-only
EOF
    edo chmod ugo+x "${IMAGE}"/etc/X11/xinit/xinitrc.d/95-nvidia-settings
    fi

    local alternatives=()

    nvidia_alternatives

    alternatives_for opengl ${PN} 1 ${alternatives[@]}

    # create /dev/nvidia-uvm{,-tools}
    install_udev_files
}

nvidia-drivers-legacy_pkg_postinst() {
    alternatives_pkg_postinst

    elog "The kernel modules source is installed into /usr/src/${PNV}/"
    elog "You will have to compile it by hand. Make sure the 'nvidia' kernel module is loaded."
    elog "Make sure you use the bfd linker to link the module, using gold"
    elog "seems to result in a broken module, so don't forget to run"
    elog "'eclectic ld set bfd' as root."
    elog "Do not forget to run 'eclectic opengl set nvidia-drivers' as root."
}

